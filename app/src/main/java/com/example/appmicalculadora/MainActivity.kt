package com.example.appmicalculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText
    private lateinit var btnIngresa : Button
    private lateinit var btnsalir : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        eventoClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
        btnsalir = findViewById(R.id.btnCerrar)
        btnIngresa = findViewById(R.id.btnIngresar)

    }
    public fun eventoClic (){
        btnIngresa.setOnClickListener(View.OnClickListener {
            var usuario : String = getString(R.string.usuario)
            var contraseña : String = getString(R.string.contraseña)
            var nombre : String = getString(R.string.nombre)

            if (txtUsuario.text.toString().contentEquals(usuario) &&
                txtContraseña.text.toString().contentEquals(contraseña)){
                val intent = Intent(this,operacionesActivity::class.java)
                intent.putExtra("usuario",nombre)
                startActivity(intent)
            }
            else{
                Toast.makeText(this,"Falto inofrmacion o corresponde al usuario",Toast.LENGTH_LONG)
            }

        })
        btnsalir.setOnClickListener(View.OnClickListener {
            finish()
        })
    }
}